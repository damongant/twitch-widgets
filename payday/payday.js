$(document).ready(function() {
	var docWidth = document.body.clientWidth;
	var textWidth = $('.marquee').width();
	$(".marquee").css('display', 'block')
	$(".pd-alert").css("width", (docWidth-45).toString() + "px")
	$.keyframe.define([{
	  name: 'marquee',
	  '0%': {'text-indent': (docWidth-45).toString() + "px"},
	  '100%': {'text-indent': (textWidth*-1).toString() + "px"}
	}]);
	$(".marquee").playKeyframe({
	  name: 'marquee', // name of the keyframe you want to bind to the selected element
	  duration: docWidth*30, // [optional, default: 0, in ms] how long you want it to last in milliseconds
	  timingFunction: 'linear', // [optional, default: ease] specifies the speed curve of the animation
	  delay: 0, //[optional, default: 0, in ms]  how long you want to wait before the animation starts in milliseconds, default value is 0
	  repeat: 'infinite',
	});

}) 